class ModulsController < ApplicationController
  before_action :set_modul, only: [:show, :update, :destroy]
  before_action :authenticate_user, only: [ :create, :update, :destroy]

  # GET /moduls
  def index
    @moduls = Modul.all

    render json: @moduls
  end

  # GET /moduls/1
  def show
    render json: @modul
  end

  # POST /moduls
  def create
    if (current_user.nama == "adminku_edupreneur_1" && current_user.admin == true)
    @modul = Modul.new(modul_params)

    if @modul.save
      render json: @modul, status: :created
    else
      render json: @modul.errors, status: :unprocessable_entity
    end
  end
end

  # PATCH/PUT /moduls/1
  def update
    if (current_user.nama == "adminku_edupreneur_1" && current_user.admin == true)
    if @modul.update(modul_params)
      render json: @modul
    else
      render json: @modul.errors, status: :unprocessable_entity
    end
  end
end

  # DELETE /moduls/1
  def destroy
    @modul.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_modul
      @modul = Modul.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def modul_params
      params.require(:modul).permit(:nama_modul, :keterangan, :mentor)
    end
end
