class MembersController < ApplicationController
  before_action :set_member, only: [:show, :update, :destroy]
  before_action :authenticate_user, only: [ :create, :update, :destroy]
  # GET /members
  def index
    @members = Member.all

    render json: @members
  end

  # GET /members/1
  def show
    render json: @member
  end

  # POST /members
  def create
    @member = current_user.members.where(user_id: current_user.id, modul_id: Modul.find(params[:id])).first_or_create(member_params)

    if @member.save
      render json: @member, status: :created
    else
      render json: @member.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /members/1
  def update
    if @member.update(member_params)
      render json: @member
    else
      render json: @member.errors, status: :unprocessable_entity
    end
  end

  # DELETE /members/1
  def destroy
    @member.destroy
  end

  def showMember
    @modul = Modul.find(params[:id])
		@member = @modul.members
		render json: { Anggota: @member },:include => {:user => {:only => :nama }}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def member_params
      params.permit(:user_id, :modul_id)
    end
end
