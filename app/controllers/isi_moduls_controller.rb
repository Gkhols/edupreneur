class IsiModulsController < ApplicationController
  before_action :set_isi_modul, only: [:show, :update, :destroy]
  before_action :authenticate_user, only: [ :create, :update, :destroy]
  # GET /isi_moduls
  def index
    @isi_moduls = IsiModul.all

    render json: @isi_moduls
  end

  # GET /isi_moduls/1
  def show
    render json: @isi_modul
  end

  # POST /isi_moduls
  def create
    if (current_user.nama == "adminku_edupreneur_1" && current_user.admin == true)
    @isi_modul = IsiModul.where(modul_id: Modul.find(params[:id])).new(isi_modul_params)
    if @isi_modul.save
      render json: @isi_modul, status: :created
    else
      render json: @isi_modul.errors, status: :unprocessable_entity
    end
  end
end

  # PATCH/PUT /isi_moduls/1
  def update
    if (current_user.nama == "adminku_edupreneur_1" && current_user.admin == true)
    if @isi_modul.update(update_params)
      render json: @isi_modul
    else
      render json: @isi_modul.errors, status: :unprocessable_entity
    end
  end
end

  # DELETE /isi_moduls/1
  def destroy
    @isi_modul.destroy
  end

  def IsiModul
		@isiModul = IsiModul.where(modul_id: Modul.find(params[:id]))
    @isimoduls = @isiModul.all
		render json: { isiModul: @isiModul }
    #code
  end

  private
    # Use callbacks to share common setup or constraints between actions.


    def set_isi_modul
      @isi_modul = IsiModul.find(params[:id])
    end


    # Only allow a trusted parameter "white list" through.
    def isi_modul_params
      params.permit(:judul,:video, :modul_id, :penjelasan)
    end
    def update_params
      params.permit(:video)
    end
end
