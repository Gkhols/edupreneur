class MentorsController < ApplicationController
  before_action :set_mentor, only: [:show, :update, :destroy]
  before_action :authenticate_user, only: [ :create, :update, :destroy]

  # GET /mentors
  def index
    @mentors = Mentor.all

    render json: @mentors
  end

  # GET /mentors/1
  def show
    render json: @mentor
  end

  # POST /mentors
  def create
    if (current_user.nama == "adminku_edupreneur_1" && current_user.admin == true)
    @mentor = Mentor.first_or_create(mentor_params)

    if @mentor.save
      render json: @mentor, status: :created
    else
      render json: {result: 'Kamu Bukan Admin', status: :unprocessable_entity}
    end
  end
end

  # PATCH/PUT /mentors/1
  def update
    if (current_user.nama == "adminku_edupreneur_1" && current_user.admin == true)
    if @mentor.update(mentor_params)
      render json: @mentor
    else
      render json: @mentor.errors, status: :unprocessable_entity
    end
  end
end

  # DELETE /mentors/1
  def destroy
    @mentor.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mentor
      @mentor = Mentor.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def mentor_params
      params.require(:mentor).permit(:nama, :spesialisasi, :pengalaman, :lokasi)
    end
end
