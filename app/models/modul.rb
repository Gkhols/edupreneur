class Modul < ApplicationRecord
  has_many :members
  has_many :user, through: :members

  has_many :isi_moduls
end
