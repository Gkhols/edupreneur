class User < ApplicationRecord
  has_secure_password
	validates :password, presence: true, allow_nil: true
	validates :email, uniqueness: true
  validates :admin, presence: true, allow_nil: true

  has_many :members
  has_many :moduls, through: :members
end
