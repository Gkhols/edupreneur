class CreateIsiModuls < ActiveRecord::Migration[5.2]
  def change
    create_table :isi_moduls do |t|
      t.string :judul
      t.string :video
      t.references :modul, foreign_key: true
      t.text :penjelasan

      t.timestamps
    end
  end
end
