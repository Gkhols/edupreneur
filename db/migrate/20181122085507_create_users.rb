class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :nama
      t.string :email
      t.string :password_digest
      t.string :no_telp
      t.string :lokasi

      t.timestamps
    end
  end
end
