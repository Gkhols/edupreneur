class CreateModuls < ActiveRecord::Migration[5.2]
  def change
    create_table :moduls do |t|
      t.string :nama_modul
      t.text :keterangan
      t.string :mentor

      t.timestamps
    end
  end
end
