class CreateMentors < ActiveRecord::Migration[5.2]
  def change
    create_table :mentors do |t|
      t.string :nama
      t.string :spesialisasi
      t.text :pengalaman
      t.string :lokasi

      t.timestamps
    end
  end
end
