# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_29_140904) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "isi_moduls", force: :cascade do |t|
    t.string "judul"
    t.string "video"
    t.bigint "modul_id"
    t.text "penjelasan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["modul_id"], name: "index_isi_moduls_on_modul_id"
  end

  create_table "members", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "modul_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["modul_id"], name: "index_members_on_modul_id"
    t.index ["user_id"], name: "index_members_on_user_id"
  end

  create_table "mentors", force: :cascade do |t|
    t.string "nama"
    t.string "spesialisasi"
    t.text "pengalaman"
    t.string "lokasi"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "moduls", force: :cascade do |t|
    t.string "nama_modul"
    t.text "keterangan"
    t.string "mentor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "nama"
    t.string "email"
    t.string "password_digest"
    t.string "no_telp"
    t.string "lokasi"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin"
    t.boolean "mentor"
  end

  add_foreign_key "isi_moduls", "moduls"
  add_foreign_key "members", "moduls"
  add_foreign_key "members", "users"
end
