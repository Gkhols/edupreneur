Rails.application.routes.draw do

  #resources :members

  #modul
  resources :moduls

  #mentor
  post 'mentor/create' => 'mentors#create'
  get 'mentor/all' => 'mentors#index'
  put 'mentor/:id/update' => 'mentors#update'
  delete 'mentor/:id/delete' => 'mentors#delete'

  #user
  post 'user/signup' => 'users#create'
  get 'user/all' => 'users#index'
  post 'user/signin' => 'user_token#create'

  #isi modul
  put 'modul/:id/update' => 'isi_moduls#update'
  get 'modul/:id/see/isimodul' => 'isi_moduls#IsiModul'
  get 'isimodul/all' => 'isi_moduls#index'
  post 'modul/:id/create/isimodul' => 'isi_moduls#create'
  delete 'isimodul/:id/delete' => 'isi_moduls#delete'

  #member
  post 'modul/:id/create/member' => 'members#create'
  get 'modul/:id/see/member' => 'members#showMember'
  delete 'modul/:id/delete/member' => 'members#delete'

  #resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
